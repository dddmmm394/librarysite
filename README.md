Перед запуском:

```
pip install django django-bootstrap-static django-forms-bootstrap psycopg2
#если юзаем постгрес (enable in settings)
у юзера admin (админ) пароль 1111
создаем БД django_db
в LibrarySite/settings.py строка 81 переключаем с sqlite на postgres (просто комментим)
#
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
```
После обновления схемы БД:

```
rm db.sqlite3
#удалить все кроме __init__.py из library\migrations
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
```
Запуск:

`python manage.py runserver`
