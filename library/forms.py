from django import forms
from library.models import Reader, Book, Author, LibraryCard, BookInLibraryCard
import re
import datetime

_human_name_regex = re.compile("[А-ЯЁ][а-яё]*('[А-ЯЁа-яё]+|[ -][А-ЯЁ][а-яё]*)*")

_selectpicker_widget=forms.Select(attrs={'data-live-search': 'true', "class": 'selectpicker'})

def is_valid_human_name(name):
    return _human_name_regex.fullmatch(name) is not None


class EmptyModelChoiceField(forms.MultipleChoiceField):
    def __init__(self, checker, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.checker = checker

    def valid_value(self, value):
        return self.checker(value)


class ReaderForm(forms.ModelForm):
    class Meta:
        model = Reader
        fields = ['surname', 'name', 'patronymic', 'passportSeries', 'passportNumber', 'phoneNumber']

        # ДЛЯ ЛОКАЛИЗАЦИИ
        labels = {
            'name': 'Имя',
            'surname': 'Фамилия',
            'patronymic': 'Отчество',
            'passportSeries': 'Серия',
            'passportNumber': 'Номер',
            'phoneNumber': 'Телефон'
        }
        help_texts = {
            'name': 'Введите имя читателя.',
            'surname': 'Введите фамилию читателя.',
            'patronymic': 'Введите отчество читателя.',
            'passportSeries': 'Введите серию паспорта.',
            'passportNumber': 'Введите номер паспорта.',
            'phoneNumber': 'Введите номер телефона в формате 84351234567.'
        }
        error_messages = {
            'name': {
                'max_length': 'Имя слишком длинное.',
                'empty': 'Поле не может быть пустым.',
                'incomplete': 'Поле не может быть пустым.',
            },
            'surname': {
                'max_length': 'Фамилия слишком длинная.',
                'empty': 'Поле не может быть пустым.'
            },
            'patronymic': {
                'max_length': 'Отчество слишком длинное.',
                'empty': 'Поле не может быть пустым.'
            },
            'passportSeries': {
                'max_length': 'Серия паспорта слишком длинная.',
                'empty': 'Поле не может быть пустым.'
            },
            'passportNumber': {
                'max_length': 'Номер паспорта слишком длинный.',
                'empty': 'Поле не может быть пустым.'
            },
            'phoneNumber': {
                'max_length': 'Номер телефона слишком длинный.',
                'empty': 'Поле не может быть пустым.'
            }
        }

    def clean(self):
        cleaned_data = super().clean()

        if not is_valid_human_name(cleaned_data["surname"]):
            raise forms.ValidationError("Недопустимое значение фамилии. Допускаются только русские буквы, пробел или \"-\" для составных фамилий. Первая буква должна быть заглавной.")

        if not is_valid_human_name(cleaned_data["name"]):
            raise forms.ValidationError("Недопустимое значение имени. Допускаются только русские буквы, пробел или \"-\" для составных имен. Первая буква должна быть заглавной.")

        if not is_valid_human_name(cleaned_data["patronymic"]):
            raise forms.ValidationError("Недопустимое значение отчества. Допускаются только русские буквы, пробел или \"-\" для составного отчества. Первая буква должна быть заглавной.")

        if not self.is_valid_series(cleaned_data["passportSeries"]):
            raise forms.ValidationError("Недопустимое значение серии паспорта")

        if not self.is_valid_passport_number(cleaned_data["passportNumber"]):
            raise forms.ValidationError("Недопустимое значение номера паспорта")

        if not self.is_valid_phone_number(cleaned_data["phoneNumber"]):
            raise forms.ValidationError("Недопустимое значение телефонного номера")
        return cleaned_data

    @staticmethod
    def is_valid_phone_number(number):
        return re.fullmatch("(\+7|8)[0-9]{10}", number) is not None

    @staticmethod
    def is_valid_series(series):
        return 0 <= series <= 9999

    @staticmethod
    def is_valid_passport_number(number):
        return 0 <= number <= 999999


class AuthorForm(forms.ModelForm):
    patronymic = forms.CharField(required=False, label="Отчество", help_text='Введите отчество автора')

    class Meta:
        model = Author
        fields = ['surname', 'name', 'patronymic']

        labels = {
            'surname': 'Фамилия',
            'name': 'Имя',
        }
        help_texts = {
            'surname': 'Введите фамилию автора',
            'name': 'Введите имя автора',
        }

    def clean(self):
        cleaned_data = super().clean()

        if not is_valid_human_name(cleaned_data["surname"]):
            raise forms.ValidationError("Недопустимое значение фамилии. Допускаются только русские буквы, пробел или \"-\" для составных фамилий. Первая буква должна быть заглавной.")

        if not is_valid_human_name(cleaned_data["name"]):
            raise forms.ValidationError("Недопустимое значение имени. Допускаются только русские буквы, пробел или \"-\" для составных имен. Первая буква должна быть заглавной.")

        if cleaned_data["patronymic"] != "" and not is_valid_human_name(cleaned_data["patronymic"]):
            raise forms.ValidationError("Недопустимое значение отчества. Допускаются только русские буквы, пробел или \"-\" для составного отчества. Первая буква должна быть заглавной.")

        if len(Author.objects.filter(name=cleaned_data["name"], surname=cleaned_data["surname"], patronymic=cleaned_data["patronymic"])) != 0:
            raise forms.ValidationError("Автор с указанным ФИО уже существует.")
        return cleaned_data


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = ["inventoryNumber", "bookName", "amount", "publishingHouse", "yearOfPublishing",  'authors']

        labels = {
            'inventoryNumber': 'Инвентарный номер',
            'bookName': 'Название',
            'amount': 'Количество',
            'publishingHouse': 'Издательство',
            'yearOfPublishing': 'Год издания'
        }
        help_texts = {
            'inventoryNumber': 'Введите инвентарный номер книги.',
            'bookName': 'Введите название книги.',
            'amount': 'Укажите количество.',
            'publishingHouse': 'Введите издательство.',
            'yearOfPublishing': 'Укажите год издания.'
        }

    authors = EmptyModelChoiceField(checker=lambda author_id: Author.objects.get(pk=author_id) is not None,
                                    label='Авторы', help_text='Выбранные авторы книги')

    all_authors = forms.ModelChoiceField(queryset=Author.objects.all(), empty_label=None, label='Доступные авторы',
                                       help_text='Выберите автора для добавления', widget=_selectpicker_widget)

    def clean(self):
        cleaned_data = super().clean()
        if (cleaned_data["amount"]) <= 0:
            raise forms.ValidationError("Неверное число книг, число книг должно быть >0")

        if (cleaned_data["yearOfPublishing"]) > datetime.datetime.now().year:
            raise forms.ValidationError("Неверный год, год должен быть меньше или равен текущему")

        author_ids = {author_id for author_id in cleaned_data["authors"]}
        new_authors = [Author.objects.get(pk=author_id) for author_id in author_ids]
        cleaned_data["authors"] = new_authors
        return cleaned_data



class SelectReaderForm(forms.Form):
    library_card = forms.ModelChoiceField(queryset=LibraryCard.objects.filter(status=True), empty_label=None, label='Читательский билет', widget=_selectpicker_widget)


class BooksChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return str(obj) + ", доступно: " + str(obj.amount - BookInLibraryCard.books_on_hands(obj))


class AddBooksForm(forms.Form):
    amount = forms.IntegerField(label='Количество', help_text='Укажите количество для добавления')
    book = BooksChoiceField(queryset=Book.objects.all(), empty_label=None, label='Книга', help_text='Выберите книгу', widget=_selectpicker_widget)

    def clean(self):
        cleaned_data = super().clean()
        amount = cleaned_data["amount"]
        if amount <= 0:
            raise forms.ValidationError("Неправильное количество книг для добавления")
        return cleaned_data


class WriteOffBooksForm(forms.Form):
    amount = forms.IntegerField(label='Количество', help_text='Укажите количество книг для списания')
    book = BooksChoiceField(queryset=Book.objects.all(), empty_label=None, label='Книга', help_text='Выберите книгу',  widget=_selectpicker_widget)

    def clean(self):
        cleaned_data = super().clean()
        book = cleaned_data["book"]
        amount = cleaned_data["amount"]
        if amount <= 0:
            raise forms.ValidationError("Неправильное количество книг для списания")
        if book.amount - BookInLibraryCard.books_on_hands(book) - amount < 0:
            raise forms.ValidationError("Указанное количество для списания книг больше, чем доступно")
        return cleaned_data


class BooksToLibraryCardForm(forms.Form):
    books = EmptyModelChoiceField(checker=lambda book_id :len(Book.objects.filter(pk= book_id)) != 0, label='Книги для выдачи', help_text='Список выбранных для выдачи книг')

    all_books = BooksChoiceField(queryset=Book.objects.all(), empty_label=None, label='Доступные книги',
                                  help_text='Выберите книгу', widget=_selectpicker_widget)

    def clean(self):
        cleaned_data = super().clean()
        try:
            books = cleaned_data["books"]
        except KeyError:
            raise forms.ValidationError("Выбрана несуществующая книга")
        new_cleaned_data = []
        total_books = {}
        for book_id in books:
            book = Book.objects.get(pk=book_id)
            if book_id in total_books:
                total_books[book_id][0] += 1
            else:
                total_books[book_id] = [1, book]
            new_cleaned_data.append(book)

        err_msg = "Недостаточно следующих книг для выдачи:\n"
        fail = False
        for amount, book in total_books.values():
            if BookInLibraryCard.books_on_hands(book) + amount > book.amount:
                err_msg += "\t%s в количестве %i;\n" % (str(book), BookInLibraryCard.books_on_hands(book) + amount - book.amount)
                fail = True
        if fail:
            raise forms.ValidationError(err_msg)

        cleaned_data["books"] = new_cleaned_data
        return cleaned_data


class ReturnBooksForm(forms.Form):
    def __init__(self, library_card, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['books'] = forms.ModelMultipleChoiceField(queryset=BookInLibraryCard.objects.filter(libraryCard=library_card, closingDate=None), label='Книги', help_text='Выберите книги для возврата')
