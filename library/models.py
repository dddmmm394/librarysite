from django.db import models
from django.db.models import Sum


class Reader(models.Model):
    surname = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    patronymic = models.CharField(max_length=100)
    passportSeries = models.IntegerField()
    passportNumber = models.IntegerField()
    phoneNumber = models.CharField(max_length=12)

    class Meta:
        indexes = [models.Index(fields=["passportSeries", "passportNumber"])]


class LibraryCard(models.Model):
    yearOfIssue = models.IntegerField()
    status = models.BooleanField()
    reader = models.ForeignKey(Reader, on_delete=models.PROTECT)

    def __str__(self):
        return "%s %s %s, %i" % (self.reader.surname, self.reader.name, self.reader.patronymic, self.id)


class Author(models.Model):
    surname = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    patronymic = models.CharField(max_length=100)

    def __str__(self):
        if self.patronymic != '':
            return "%s %s %s" % (self.surname, self.name, self.patronymic)
        else:
            return "%s %s" % (self.surname, self.name)

    def short_name(self):
        if self.patronymic != '':
            return "%s %s. %s." % (self.surname, self.name[0], self.patronymic[0])
        else:
            return "%s %s." % (self.surname, self.name[0])

    class Meta:
        indexes = [models.Index(fields=["surname", "name", "patronymic"])]
        unique_together = ('surname', 'name', 'patronymic')


class Book(models.Model):
    inventoryNumber = models.CharField(max_length=100, primary_key=True)
    bookName = models.CharField(max_length=500, unique=True, db_index=True)
    authors = models.ManyToManyField(Author)
    amount = models.IntegerField()
    publishingHouse = models.CharField(max_length=100)
    yearOfPublishing = models.IntegerField()

    def __str__(self):
        authors = ""
        for author in self.authors.all():
            authors += author.short_name() + ", "
        return '"%s", Авторы: %sизд. %s, %i год, номер: %s' % (self.bookName, authors, self.publishingHouse, self.yearOfPublishing, self.inventoryNumber)
                             # ", " от авторов

    @staticmethod
    def exist_books_with_author(author):
        return len(Book.objects.filter(authors__in=[author])) != 0


class BookInLibraryCard(models.Model):
    libraryCard = models.ForeignKey(LibraryCard, on_delete=models.PROTECT)
    book = models.ForeignKey(Book, on_delete=models.PROTECT)
    amount = models.IntegerField()
    creationDate = models.DateField()
    closingDate = models.DateField(null=True)

    @staticmethod
    def books_on_hands(book):
        total = BookInLibraryCard.objects.filter(book=book, closingDate=None).aggregate(Sum('amount'))["amount__sum"]
        return total if total is not None else 0

    def __str__(self):
        return str(self.book)
