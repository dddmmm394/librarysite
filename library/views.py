from django.views.generic.edit import CreateView
from django.views.generic.edit import FormView
from django.views.generic.base import TemplateView, ContextMixin
import library.forms as forms
import library.models as models
from django.contrib.auth.mixins import PermissionRequiredMixin
from datetime import date


class PermissionsMenuCheckerMixin(ContextMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({"can_use_ReaderCreate": self.request.user.has_perms(ReaderCreate.permission_required)})
        context.update({"can_use_AuthorCreate": self.request.user.has_perms(AuthorCreate.permission_required)})
        context.update({"can_use_BookCreate": self.request.user.has_perms(BookCreate.permission_required)})
        context.update({"can_use_ChangeBookAmount": self.request.user.has_perms(AddBooks.permission_required)})
        context.update({"can_use_SelectReader": self.request.user.has_perms(SelectReader.permission_required)})
        context.update({"user_is_staff": self.request.user.is_staff})

        if self.request.user.is_authenticated:
            context.update({"user_first_name": self.request.user.first_name})
            context.update({"user_last_name": self.request.user.last_name})
        return context


class ReaderCreate(PermissionRequiredMixin, PermissionsMenuCheckerMixin, FormView):
    permission_required = ('library.add_reader', 'library.add_librarycard')
    form_class = forms.ReaderForm
    success_url = "/"
    template_name = "library/reader_form.html"

    def form_valid(self, reader_form):
        new_reader = reader_form.save()
        library_card = models.LibraryCard()
        library_card.reader = new_reader
        library_card.status = True
        library_card.yearOfIssue = date.today().year
        library_card.save()
        return super().form_valid(reader_form)


class AuthorCreate(PermissionRequiredMixin, PermissionsMenuCheckerMixin, CreateView):
    permission_required = ['library.add_author']
    form_class = forms.AuthorForm
    model = models.Author
    success_url = "/"


class BookCreate(PermissionRequiredMixin, PermissionsMenuCheckerMixin, FormView):
    permission_required = ['library.add_book']
    form_class = forms.BookForm
    success_url = "/"
    template_name = "library/book_form.html"

    def form_valid(self, book_form):
        book_form.save()
        return super().form_valid(book_form)


class AddBooks(PermissionRequiredMixin, PermissionsMenuCheckerMixin, FormView):
    permission_required = ('library.change_book', 'library.delete_book', 'library.delete_author')
    form_class = forms.AddBooksForm
    success_url = "/"
    template_name = "library/change_book_amount.html"

    def form_valid(self, add_books):
        book = add_books.cleaned_data["book"]
        amount = add_books.cleaned_data["amount"]
        book.amount += amount
        book.save(force_update=True, update_fields=['amount'])
        return super().form_valid(add_books)


class WriteOffBooks(PermissionRequiredMixin, PermissionsMenuCheckerMixin, FormView):
    permission_required = ('library.change_book', 'library.delete_book', 'library.delete_author')
    form_class = forms.WriteOffBooksForm
    success_url = "/"
    template_name = "library/change_book_amount.html"

    def form_valid(self, write_off_books):
        book = write_off_books.cleaned_data["book"]
        amount = write_off_books.cleaned_data["amount"]
        book.amount -= amount
        if book.amount == 0:
            authors = [author for author in book.authors.all()]
            book.delete()
            for author in authors:
                if not models.Book.exist_books_with_author(author):
                    author.delete()
        else:
            book.save(force_update=True, update_fields=['amount'])
        return super().form_valid(write_off_books)


class SelectReader(PermissionRequiredMixin, PermissionsMenuCheckerMixin, FormView):
    permission_required = ['library.change_librarycard']
    form_class = forms.SelectReaderForm
    template_name = "library/select_reader_form.html"

    def get_success_url(self):
        return "/reader/%i" % self.library_card_id

    def form_valid(self, library_card_form):
        library_card = library_card_form.cleaned_data["library_card"]
        self.library_card_id = library_card.id
        return super().form_valid(library_card_form)


class AddBooksToLibraryCard(PermissionRequiredMixin, PermissionsMenuCheckerMixin, FormView):
    permission_required = ['library.add_bookinlibrarycard']
    form_class = forms.BooksToLibraryCardForm
    template_name = "library/books_to_libcard_form.html"

    def get_success_url(self):
        return "/reader/%i" % self.kwargs["libcard_id"]

    def form_valid(self, add_books_form):
        books = add_books_form.cleaned_data["books"]
        library_card = models.LibraryCard.objects.get(pk=self.kwargs["libcard_id"])
        for book in books:
            book_in_card = models.BookInLibraryCard()
            book_in_card.libraryCard = library_card
            book_in_card.book = book
            book_in_card.amount = 1
            book_in_card.creationDate = date.today()
            book_in_card.save()
        return super().form_valid(add_books_form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        libcard_id = self.kwargs["libcard_id"]
        context.update({"libcard_id": libcard_id})
        return context


class ReturnBooks(PermissionRequiredMixin, PermissionsMenuCheckerMixin, FormView):
    permission_required = ['library.change_bookinlibrarycard']
    form_class = forms.ReturnBooksForm
    template_name = "library/return_books_form.html"

    def get_success_url(self):
        return "/reader/%i" % self.kwargs["libcard_id"]

    def get_form_kwargs(self):
        args = super().get_form_kwargs()
        args.update({'library_card': models.LibraryCard.objects.get(pk=self.kwargs['libcard_id'])})
        return args

    def form_valid(self, add_books_form):
        books = add_books_form.cleaned_data["books"]
        for book in books:
            book.closingDate = date.today()
            book.save(force_update=True, update_fields=['closingDate'])
        return super().form_valid(add_books_form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        libcard_id = self.kwargs["libcard_id"]
        context.update({"libcard_id": libcard_id})
        return context


class ReaderIndexPage(PermissionsMenuCheckerMixin, TemplateView):
    template_name = "library/reader_index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        libcard_id = self.kwargs["libcard_id"]
        libcard = models.LibraryCard.objects.get(id=libcard_id)
        context.update({"libcard_id": libcard_id,
                        "libcard": libcard, "reader": libcard.reader,
                        "passportSeries": "{0:04d}".format(libcard.reader.passportSeries),
                        "passportNumber": "{0:06d}".format(libcard.reader.passportNumber),
                        })
        return context


class IndexPage(PermissionsMenuCheckerMixin, TemplateView):
    template_name = "library/index.html"


