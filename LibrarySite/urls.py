"""LibrarySite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from django.conf.urls import include, url
import library.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', library.views.IndexPage.as_view()),
    path('add_reader/', library.views.ReaderCreate.as_view()),
    path('add_author/', library.views.AuthorCreate.as_view()),
    path('add_book/', library.views.BookCreate.as_view()),
    path('select_reader/', library.views.SelectReader.as_view()),
    path('reader/<int:libcard_id>/', library.views.ReaderIndexPage.as_view()),
    path('reader/<int:libcard_id>/get_books/', library.views.AddBooksToLibraryCard.as_view()),
    path('reader/<int:libcard_id>/return_books/', library.views.ReturnBooks.as_view()),
    path('add_books/', library.views.AddBooks.as_view()),
    path('write_off_books/', library.views.WriteOffBooks.as_view()),
    path('accounts/login/', LoginView.as_view(template_name='library/login.html', redirect_field_name='/')), # authentication_form=Form https://docs.djangoproject.com/en/2.0/topics/auth/default/#django.contrib.auth.forms.AuthenticationForm
    path('accounts/logout/', LogoutView.as_view(next_page='/accounts/login/'))
]
